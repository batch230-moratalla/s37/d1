const mongoose = require("mongoose");
const Course = require("../models/course");

// Function for adding a course

module.exports.addCourse = (reqBody) => {

  let newCourse = new Course({
    name: reqBody.name,
    description:reqBody.description,
    price: reqBody.price,
		slots: reqBody.slots
  })

  return newCourse.save().then((newCourse, error) => {
    if(error){
      return error;
    }
    else{
      return newCourse;
    }
  })
}

/* 
module.exports.addCourse = (reqBody, result) => {
	if(result.isAdmin == true){
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
	})
	return newCourse.save().then((newCourse, error) =>
		{
			if (error){
				return error;		
			}
			else{
				return newCourse;
			}
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
} 
*/

// GET all courses
module.exports.getAllCourse = () => {
  return Course.find({}).then(result => {
    return result;
  })
}

// GET all active Courses
module.exports.getActiveCourses = () => {
  return Course.find({isActive:true}).then(result => {
    return result;
  })
}

// GET specific course
module.exports.getCourse = (courseId) => {
	return Course.findById(courseId).then(result =>{
		return result;
	})
}

// [Additional Example]
// [Arrow function to regular function]
// Get ALL course feature converted to regular function
// 'GetSpecificCourseInRegularFunction' acts as a variable/storage of getCourseV2() function so it could be exported
module.exports.GetSpecificCourseInRegularFunction = 
function getCourseV2(courseId){
	return Course.findById(courseId).
	then(
		function sendResult(result){
			return result;
		}
	);
}


// GET Specific Course
/* 
module.exports.getCourse = (courseId) => {
  return Course.findById(courseId).then(result => {
    return result; // wala dapat bracket "{}" ({}) pag find by id
  })
}

// UPDATING a Course
module.exports.updateCourse = (courseId, newData) => {
  if(newData.isAdmin == true){
    return Course.findByIdAndUpdate(courseId, {
      //newData.course.name
      //newData.request.body
      name: newData.course.name,
      description: newData.cource.description,
      price: newData.course.price
    })
    .then((result, error) => {
      if(error){
        return false;
      }
      return true
    })
  }
  else{
    let message = Promise.resolve(`User mush be ADMIN to access this functionality`);
    return message.then((value) => {return value})
  }
}
 */

// Updating a Course
module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				// newData.course.name
				// newData.request.body.name
				name: newData.course.name, 
				description: newData.course.description,
				price: newData.course.price
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true;
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}

// Promise.resolve = if this is not on the c

// activity 2. update the "addCourse" controller method to implement admin authentication for creating a courses

// NOTE: [1] include screenshot of successful admin addCourse and [2] screenshot of not successful add course by a user that is not admin


// Create a route and a controller method
// controller is part of route 
// create a feature that is limited to admin user to deactive a course

// Create a route for achiving a ourse, the route must use jwt

// to get id from url = use params


/* 
S39 Activity Instructions:
1. Refactor the course route to implement user authentication for the admin when creating a course.
2. Refactor the addCourse controller method to implement admin authentication for creating a course.
3. Create a git repository named S34.
4. Add another remote link and push to git with the commit message of Add activity code - S39.
5. Add the link in Boodle.

NOTE: [1] include screenshot of successful admin addCourse and [2] screenshot of not successful add course by a user that is not admin
S39 Expected Output:

*/

/* 
S40 - Activity
1. Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url.
2. Create a controller method for archiving a course obtaining the course ID from the request params and the course information from the request body.
3. Process a PUT request at the /courseId/archive route using postman to archive a course
4. Create a git repository named S40.
5. Add another remote link and push to git with the commit message of Add activity code - S40.
6. Add the link in Boodle.
*/

module.exports.archiveCourse = (courseId, ArchiveData) => {
	if(ArchiveData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				isActive: ArchiveData.course.isActive,
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true;
		})
	}
	else{
		let message = Promise.resolve('User must be an ADMIN to change Course status');
		return message.then((value) => {return value});
	}
}

// Function for enrollment 