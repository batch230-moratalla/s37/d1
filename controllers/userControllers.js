const User = require("../models/user");
const Course = require("../models/course")
const bcrypt = require("bcrypt");
const auth = require("../auth");



module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    firstName : reqBody.firstName,
    lastName : reqBody.lastName,
    email : reqBody.email,
    // isAdmin: reqBody.isAdmin,
    mobileNumber: reqBody.mobileNumber,
    password : bcrypt.hashSync(reqBody.password, 10)  // 10 - salt
  })
  return newUser.save().then((user, error) => {
    if(error){
      return false;
    }
    else{
      return newUser; // return true;
    }
  })
}

/* research for unable to register existing user
$or: [{username: request.body.username},
{email: request.body.email},
{mobile: request.body.mobile}]
 tapos pasok sa if yung pag check if exist \na yung username, email, or mobile number
sa else yung registration

module.exports.registerUser = (request, response) => {
    UserModels.findOne({
        $or: [{username: request.body.username},
            {email: request.body.email},
            {mobile: request.body.mobile}]
        }).then(result => {
            console.log(result);
            if(result){
                if(result != null && result.username == request.body.username){
                    return response.send("Username already exist")
                }
                else if(result != null && result.email == request.body.email){
                    return response.send("Email already exist")
                }
                else{
                    return response.send("Mobile number already exist")
                } 
            }
            else{
                let newUser = new UserModels({
                    username: request.body.username,
                    name: request.body.name,
                    email: request.body.email,
                    password: bcrypt.hashSync(request.body.password, 10),
                    mobile: request.body.mobile
                })
                newUser.save((saveError, savedUser) => {
                if(saveError){
                    return console.error(saveError);
                }
                else{
                    return response.status(201).send(Registration Successful);
                }
            });
        }
    })
    .catch(error => response.send(error));
}

// for capstone 2 return true - return response.status(201).send(Registration Successful);

module.exports.registerUserController3 = (req, res) =>{
    const emailChecker = User.findOne({email:req.body.email});
    const mobileNoChecker = User.findOne({MobileNo:req.body.mobileNo});
    if(emailChecker != null || mobileNoChecker != null){
        
    }
}
*/

// Check email exist
module.exports.checkEmailExist = (reqBody) => {
  return User.find({email: reqBody.email}).then(result => {
    if(result.length > 0){
      return result;
    }
    else{
      return false;
      // saving process
    }
  })
}

// login
module.exports.logInUser = (reqBody) => {
  return User.findOne({email: reqBody.email}).then(result => {
    if(result == null){
      return false;
    }
    else{
      // compareSync is a bcrypt function to compare unhashed password to hashed password
      const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password) // return true or false / pag match xa true, else false
      if(isPasswordCorrect){
        return {"Tama ang Password mo, please copy this codes": auth.createAccessToken(result)}; // return {Access: auth.createAccessToken(result)};
        // let's give the user a token to acces feature
      }
      else{
        // If password does not match, else
        return "Mali ang Password mo! Please try again"; // return false;
      }
    }
  })
}


/* 
  1. Create a /details route that will accept the user’s Id to retrieve the details of a user.
  2. Create a getProfile controller method for retrieving the details of the user:
  a. Find the document in the database using the user's ID
  b. Reassign the password of the returned document to an empty string
  c. Return the result back to the frontend
  3. Process a POST request at the /details route using postman to retrieve the details of the user.
 */
// s38-activity 
/* copy of my soln
  module.exports.getProfile = (reqBody) => {
    return User.findOne({email: reqBody.email}).then((result) => {
        result.password = "*****"
        return result;
      })
    }
 */

module.exports.getProfile = (request, response) => {
  // Will contain your decode token
  const userData = auth.decode(request.headers.authorization);

  console.log(userData);

  return User.findById(userData.id).then(result => {
    result.password = "******"
    response.send(result);
  })
}

// enroll feature
module.exports.enroll = async (request, response) => {

  const userData = auth.decode(request.headers.authorization);

  let courseName = await Course.findById(request.body.courseId).then(result => result.name);

  let newData = {
      // userId and email will be retrieve from the request header (request header contains  the user token)
      userId: userData.id,
      email: userData.email,
      // courseId will be retrieved from the request.body
      courseId: request.body.courseId,
      courseName: courseName
  };
  console.log(newData);

    // perfect method to add object to an array is to use push
  let isUserUpdated = await User.findById(newData.userId)
    .then(user => {
      user.enrollments.push({
        courseId: newData.courseId,
        courseName: newData.courseName,
    });
    // save the updated user infrmation from the database
    return user.save()
    .then(result => {
        console.log(result);
        return true;
    })
    .catch(error => {
        console.log(error);
        return false;
    })
  })
  console.log(isUserUpdated);

  let isCourseUpdated = await Course.findById(newData.courseId).then(course => {
    course.enrollees.push({
        userId: newData.userId,
        email: newData.email
    })

    // Minuse the slots avaialbe by 1
    // course.slots = course.slots -1;
		// Mini Activity -
		// [1] Create a condition that if the slots is already zero, no deduction of slot will happen and...
		// [2] it should have a message in the terminal that the slot is already zero
		// [3] Else if the slot is negative value, it should make the slot value to zero
    /* 
      let numSlots = (course.slots -= 1); 
     if(numSlots == 0 && Math.sign(numSlots) == -1){
       return "Unable to enrol, no more slots available"
     }
     else{
       
     }
    */
    let numSlots = course.slots ; 
    course.slots -= 1
    if(numSlots == 0 || Math.sign(numSlots) == -1){
      return "Unable to enrol, no more slots available"
    }
    else{
      return course.save()

    .then(result => {
      console.log(result);
      return true;
    })
    .catch(error => {
      console.log(error);
      return false;
    })
    }
    
  })
  console.log(isCourseUpdated);

    // Condition will check if the both "user and course" document has been updated
    // Ternary operator
    // (isUserUpdated == true && isCourseUpdated == true)? response.send(true) : response.send(false);
  (isUserUpdated == true && isCourseUpdated == true)? response.send("You are now enrolled") : response.send("Sorry there was an error");
    /* 
    if(isUserUpdated == true && isCourseUpdated == true){
      ressponse.send(true);
    }
    else{
      response.send(false);
    }
    */
}

/* 
module.exports.enroll = async (request, response) => {

  const userData = auth.decode(request.headers.authorization);

  let courseName = await Course.findById(request.body.courseId).then(result => result.name);

  let newData = {
      // userId and email will be retrieve from the request header (request header contains  the user token)
      userId: userData.id,
      email: userData.email,
      // courseId will be retrieved from the request.body
      courseId: request.body.courseId,
      courseName: courseName
  };
  console.log(newData);

    // perfect method to add object to an array is to use push
  let isUserUpdated = await User.findById(newData.userId)
    .then(user => {
      user.enrollments.push({
        courseId: newData.courseId,
        courseName: newData.courseName,
    });
    // save the updated user infrmation from the database
    return user.save()
    .then(result => {
        console.log(result);
        return true;
    })
    .catch(error => {
        console.log(error);
        return false;
    })
  })
  console.log(isUserUpdated);

  let isCourseUpdated = await Course.findById(newData.courseId).then(course => {
    course.enrollees.push({
        userId: newData.userId,
        email: newData.email
    })

    // Minuse the slots avaialbe by 1
    // course.slots = course.slots -1;
  
    course.slots -= 1; 

    return course.save()

    .then(result => {
      console.log(result);
      return true;
    })
    .catch(error => {
      console.log(error);
      return false;
    })
  })
  console.log(isCourseUpdated);

    // Condition will check if the both "user and course" document has been updated
    // Ternary operator
    // (isUserUpdated == true && isCourseUpdated == true)? response.send(true) : response.send(false);
  (isUserUpdated == true && isCourseUpdated == true)? response.send("You are now enrolled") : response.send("Sorry there was an error");
    /* 
    if(isUserUpdated == true && isCourseUpdated == true){
      ressponse.send(true);
    }
    else{
      response.send(false);
    }
  
  }
*/



/* 
module.exports.getProfile = (reqBody) => {
    return User.findById({_id: reqBody._id}).then(result => {
      if(result == null){
        return false;
      }
      else{
        console.log(result);
        return result;
      }
  })
}
 */
/* 

module.exports.getProfile = (reqBody) => {
  return User.findById(reqbody._id).then((result, err) => { // laman ni result is an object na nagmatch sa id
    if(err){
      return false;
    }
    else{
      result.password = "*****"
      return result;
    }
  }) // dahil hindi namn tau magsearch sa one object
}
 */

// for capstone 2 make sure that you study how to change the user role as an admin, "Only the role" - so no need to register any other user

// Create a route and a controller method
// controller is part of route 
// create a feature that is limited to admin user to deactive a course


// GET all User
module.exports.getAllUser = () => {
  return User.find({}).then(result => {
    return result;
  })
};

// Deleting a User 
module.exports.deleteUser = (req, res) => {
	console.log(req.params.taskId); 
	User.findByIdAndRemove(req.params.taskId)
	.then(result => res.send(result))
  .catch(error => res.send(error))
}
