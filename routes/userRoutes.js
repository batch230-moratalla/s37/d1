const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");
const { model } = require("mongoose");


router.post("/register", (request, response) => {
  userController.registerUser(request.body).then(resultFromController => response.send(resultFromController))
})

// .post kasi may kukunin tau sa request.body
router.post("/checkEmail", (request, response) => {
  userController.checkEmailExist(request.body).then(resultFromController => response.send(resultFromController))
})

// dahilt mag loglogin tau
router.post("/login", (request, response) => {
  userController.logInUser(request.body).then(resultFromController => response.send(resultFromController))
})



//if error is cannot post check routes

// activity
/* 
router.post("/details", (request, response) => {
  userController.getProfile(request.body).then(resultFromController => response.send(resultFromController))
})
 */
// // s38 activity
// router.post("/details", (request, response) => {

// })

router.get("/allUser", (request, response) => {
  userController.getAllUser().then(resultFromController => response.send(resultFromController))
})

/* 
router.delete("/deleteUser/:taskId", (request, response) => {
  userController.getAllUser().then(resultFromController => response.send(resultFromController))
}) */
// S38
/* 
router.post("/details", (request, response) => {
  userController.getProfile(request.body).then(resultFromController => response.send(resultFromController))
})
 */
// Kaya nakapag get tau kasi ung id galing sa token
router.get("/details", auth.verify, userController.getProfile);

// enroll feature
router.post("/enroll", auth.verify, userController.enroll);

// Delete User

router.delete("/removeUser/:taskId", userController.deleteUser)

module.exports = router;