const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers")
const auth = require("../auth")


router.post("/create", (request, response) => {
  courseControllers.addCourse(request.body).then(resultFromController => response.send(resultFromController))
})
/*
router.post("/create", auth.verify, (request, response) => 
{
		const result = {
			course: request.body, 	
			isAdmin: auth.decode(request.headers.authorization).isAdmin
		}
	courseControllers.addCourse(request.body, result).then(resultFromController => response.send(resultFromController));
})  
*/

router.get("/all", (request, response) => {
  courseControllers.getAllCourse().then(resultFromController => response.send(resultFromController))
})

// Get active courses
router.get("/active", (request, response) => {
  courseControllers.getActiveCourses().then(resultFromController => response.send(resultFromController))
})

// Get Specific courses
router.get("/:courseId", (request, response) => {
  courseControllers.getCourse(request.params.courseId).then(resultFromController => response.send(resultFromController)) // dahil hindi xa galing kay req.body, gumamit tau ng params. Alright!
}) 

// [Additional Example]
// [Arrow function to regular function]
// Get SPECIFIC course
router.get("/:courseId/getSpecificV2", (request, response) => {
	courseControllers.GetSpecificCourseInRegularFunction(request.params.courseId)
	.then(
		function getResultFromController(resultFromController){
		response.send(resultFromController)
		}
	);
})


/* router.patch("/:courseId/update", (request, response) => {
  const newData = {
    course: request.body,
    isAdmin: auth.decode(request.headers.authorization).isAdmin // from auth.js
  }
  // newData.course.name
  // newData.request.body.name
  courseControllers.updateCourse(request.params.courseId).then(resultFromController => response.send(resultFromController)) 
}) */

router.patch("/:courseId/update", auth.verify, (request,response) => 
{
	const newData = {
		course: request.body, 	//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseControllers.updateCourse(request.params.courseId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})


/* 
S39 Activity Instructions:
1. Refactor the course route to implement user authentication for the admin when creating a course.
2. Refactor the addCourse controller method to implement admin authentication for creating a course.
3. Create a git repository named S34.
4. Add another remote link and push to git with the commit message of Add activity code - S34.
5. Add the link in Boodle. 
*/

router.patch("/:courseId/addCourse", auth.verify, (request,response) => 
{
	const newData = {
		course: request.body, 	//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseControllers.updateCourse(request.params.courseId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})


// S40 - activity

router.put("/:courseId/archive", auth.verify, (request,response) => 
{
	const newData = {
		course: request.body, 
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseControllers.archiveCourse(request.params.courseId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})

module.exports = router

