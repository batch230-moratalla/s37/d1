/*
  session starting now we will now
  Booking system MVP requirements - Booking system API - Business use case translation to model design
  Booking system MVP requirements
    - Minimum Viable Product is a product that
  Course booking system MVP requirements
  - Our booking system MVP must have the ff feature:
    - User registration
    - Use authentication (login)
    - Retrieval of authenticated user's details
    - Course creation by an authenticated user
    - Retrieval of courses / verification if it was offered
    - Course info updating by an authenticated user / admin
  Booking System - Besides express and mongoose, we have the following additional package
    - bcrypt - for hasing user passwords prior to storage - hashes the password, conver password to alphanumeric password
    - cors - for alowing cross-origin resource sharing / make it private, make a restriction for accessing the website
    - jsonwebtoken (JWT)- for implementing JSON web tokens / kelangan nya ng token, required ang token para makaaccess sa page / you cannot access a webpage further if you do not have token 
  User Registration - the process of creating a new user documents in the users collection, once all required information has been received
  User authentication - process of verifying that a users identy by the help of JWT
  JWT - 
*/

/*
npm init -y - initialize
npm install express
npm install mongoose
npm install cors
touch .gitignore
*/
// Server Application process template

// Dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors")

const userRoutes = require("./routes/userRoutes.js")
const courseRoutes = require("./routes/courseRoutes")


// To create an express server/application
const app = express();


// Middlewares - aows to bridge our backend application (server) to our front end

// To allow cross origin resource sharing
app.use(cors());

// To read JSON objects
app.use(express.json());


app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

// To read forms
app.use(express.urlencoded({extended:true})); // kahit anong type gagana pa din xa for "urlencoded" - search google for more info

// Connect to our MongoDB database
mongoose.connect("mongodb+srv://admin:admin@batch230.1dtuoqz.mongodb.net/courseBooking?retryWrites=true&w=majority",{
    useNewUrlParser: true,
    useUnifiedTopology:true
})
mongoose.connection.once("open", () => console.log("Now connected to Moratalla-Mongo DB Atlas"))

app.listen(process.env.PORT || 4000, () => { // "process.env" - currently we are working on local repository, ngaun pag nagonline na tau, ibang environement na xa, that environt has a port that has a different number / para mag run pa rin xa dun sa inaplodan natin ng project, gagana pa din xa pag ginamit natin si "process.env.PORT" - search google for more info
  console.log(`API is now online on port ${process.env.PORT || 4000}`)
})


















