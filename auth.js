// Goal is to generate ticket or token
// if a packege is not installed = cannot find modulen
const { response } = require("express");
const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

// Token Creator
module.exports.createAccessToken = (user) => {
  // payload
  const data = {
    id: user._id, // "._id : "1092dsfy8o2q3n8ddfs"
    email: user.email,
    isAdmin: user.isAdmin
  }
  return jwt.sign(data, secret, {/* expiresIn: "60s" */}) // secret = signature / secret key - it has a content of payload and secret key
}

// Token Verifier
// To verify a token from request (from cient/postman)
module.exports.verify = (request, response, next) => {
	// Get JWT (JSON Web Token) from postman
	let token = request.headers.authorization
	if(typeof token !== "undefined"){
		console.log(token);

		// removes the first characters ("Bearer ") from the token
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) =>{
			if(error){
				return response.send({
					auth: "Failed. "
				});
			}
			else{
				next();
			}
		})
	}
}

// To decode a token / token decoder
// To decode the user details from the token
module.exports.decode = (token) => {

	if(typeof token !== "undefined"){

		// remove first 7 characters ("Bearer ") from the token
		token = token.slice(7, token.length);
	}

	return jwt.verify(token, secret, (error, data) => {
		if(error){
			return null
		}
		else{
			return jwt.decode(token, {complete:true}).payload
		}
	})
}

/* module.exports.verify(request, response, next) => {
  // Get JWT (JSON Web Token) we will get this from our postman
  let token = request.headers.authorization
  if(typeof token !== "undefined"){
    console.log(token);

    // removes the first characters ("Bearer ") from the token
    token = token.slice(7, token.length)

    return jwt.verify(token, secret, (error, data))=> {
      if(error){
        return response.send({
          auth: "Failed. "
        })
      }
    })
  }
} */